var loginBtn = document.querySelector(".login");
var modalLoginView = document.querySelector(".modal-content");
var closeBtn = document.querySelector(".modal-content-close");

var login = modalLoginView.querySelector("[name=login]");
var form = modalLoginView.querySelector("form");
var password = modalLoginView.querySelector("[name=password]");
var storage = localStorage.getItem("login");//localStorage - храниение данных в Браузере. текущее значение того, что
// находится в localStorage

var mapOpen = document.querySelector(".js-showMap");
var modalMapView = document.querySelector(".modal-content-map");
var mapClose = modalMapView.querySelector(".modal-content-close");


//Контроль события кнопки "Как проехать"
mapOpen.addEventListener("click", function (event) {
    event.preventDefault();
    modalMapView.classList.add("modal-content-show");
});

//Контроль события кнопки "Закрыть" в "Как проехать"
mapClose.addEventListener("click", function (event) {
    event.preventDefault();
    modalMapView.classList.remove("modal-content-show");
})


//Контроль события кнопки Войти
loginBtn.addEventListener("click", function (event) {
    event.preventDefault();//отменяем дейстие по-умолчанию  href Делать такую подстраховку всегда!
    modalLoginView.classList.add("modal-content-show");
    if (storage) {    //устанавливаем значение в storage, если оно есть(юзерр ввел в логин что-то)
        login.value = storage;
        password.focus();
    } else {
        login.focus();//делаем фокус на поле login
    }
});

//Контроль события кнопки закрытия
closeBtn.addEventListener("click", function (event) {
    event.preventDefault();
    modalLoginView.classList.remove("modal-content-show");
    modalLoginView.classList.remove("modal-error");//удаляем shake
});

//Контроль события подписки
form.addEventListener("submit", function (event) {
    if (!login.value || !password.value) {
        event.preventDefault();
        modalLoginView.classList.add("modal-error"); //добавили shake если не ввили логи/пароль + надо его удалить,
        // чтобы не было глюков!-вверху удалили
        console.log("no login");
    } else {
        //сохраняем в localStorage  по ключу login
        localStorage.setItem("login", login.value);
    }
});

//Контроль события нажатия на кнопку "keydown" escape для закрытия окна
window.addEventListener("keydown", function (event) {
    if (event.keyCode === 27) {
        if (modalLoginView.classList.contains("modal-content-show")) {
            modalLoginView.classList.remove("modal-content-show");
            modalLoginView.classList.remove("modal-error");//удаляем shake
        }
    }
})